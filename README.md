# autosave conda recipe

Home: https://github.com/epics-modules/autosave

Package license: EPICS Open License

Recipe license: BSD 3-Clause

Summary: APS BCDA synApps module: autosave

## Autosave settings

We try to enclose most possible autosave configuration within `autosave.iocsh`.

## Preparation of DB

Each record in database files should have the one of following `info` tag. Such as
```
 info(autosaveFields, "PREC SCAN DESC OUT")
 info(autosaveFields_pass0, "VAL")
 info(autosaveFields_pass1, "VAL")
```

Each info tag should be matched with what one would like to use such as


* `autosaveFields` : before record initialization
  - `settings_pass0.sav` : in `$(AS_TOP)/$(IOCNAME)/save`
  - `settings_pass0.req` : in `$(AS_TOP)/$(IOCNAME)/req`

* `autosaveFields_pass0` : before record initialization
  - `values_pass0.sav` : in `$(AS_TOP)/$(IOCNAME)/save`
  - `values_pass0.req` : in `$(AS_TOP)/$(IOCNAME)/req`

* `autosaveFields_pass1`: after record initialization
  - `values_pass1.sav`    : in `$(AS_TOP)/$(IOCNAME)/save`
  - `values_pass1.req`    : in `$(AS_TOP)/$(IOCNAME)/req`

Please loot at the example in [Autosave DB example](template/SR_test_info.db).

## How to enable it within e3


```
require autosave

epicsEnvSet("TOP", "$(E3_CMD_TOP)/..")
epicsEnvSet("IOCNAME", "as_test")

iocshLoad("$(autosave_DIR)/autosave.iocsh", "AS_TOP=$(TOP),IOCNAME=$(IOCNAME)")
```
